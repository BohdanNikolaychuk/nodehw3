const express = require('express');

const router = express.Router();
const {
  getLoad,
  createLoad,
  getLoadActive,
  updateLoadActive,
  getLoadById,
  createLoadById,
  updateMyLoadById,
  deleteLoadById,
  getShippindInfo,
} = require('../controllers/loadController');

router.post('/', createLoad);
router.get('/', getLoad);
router.get('/active', getLoadActive);
router.patch('/active/state', updateLoadActive);
router.get('/:id', getLoadById);
router.put('/:id', updateMyLoadById);
router.delete('/:id', deleteLoadById);
router.post('/:id/post', createLoadById);
router.get('/:id/shipping_info', getShippindInfo);

module.exports = {
  loadRouter: router,
};
