const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');
require('dotenv').config();



const { authRouter } = require('./routers/authRouter');
const { userRouter } = require('./routers/userRouter');
const { truckRouter } = require('./routers/truckRouter');
const { loadRouter } = require('./routers/loadRouter');

const { authMiddleware } = require('./middlewares/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', authMiddleware, userRouter);
app.use('/api/trucks', authMiddleware, truckRouter);
app.use('/api/loads', authMiddleware, loadRouter);

const start = async () => {
  try {
    await mongoose.connect(process.env.DB);
    app.listen(8080, () => {
      console.log(`Server started on ${8080}`);
    });
  } catch (err) {
    console.error(`Error on server`);
  }
};

start();

