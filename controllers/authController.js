const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User, userValide } = require('../models/User');

const register = async (req, res) => {
  try {
    const { email, password, role } = req.body;
    await userValide.validateAsync({ email, password, role });
    const user = new User({
      email,
      password: await bcryptjs.hash(password, 10),
      role,
    });
    await user.save();

    return await res.status(200).json({
      message: 'Profile created successfully',
    });
  } catch (error) {
    res.status(400).json({
      message: 'Error!',
    });
  }
};

const login = async (req, res) => {

  const { email, password } = req.body
  const user = await User.findOne({ email });

  if (user && (await bcryptjs.compare(String(password), String(user.password)))) {
    const payload = {
      email: user.email,
      role: user.role,
      userId: user._id,
      createdDate: user.createdDate,
    };
    const jwtToken = jwt.sign(payload, process.env.secret_jwt_key);
    return res.status(200).json({ jwt_token: jwtToken });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

module.exports = {
  register,
  login,
};
