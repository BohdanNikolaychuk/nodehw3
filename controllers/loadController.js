const { Load, validateLoad } = require('../models/Load');
const { Truck } = require('../models/Truck');
const { getTruckForLoad } = require('../services/loadService');

const getLoad = async (req, res, next) => {
  if (req.user.role === 'DRIVER') {
    let loadsDriver = await Load.find({ assigned_to: req.user.userId, status: 'ASSIGNED' });
    return res.status(200).json({
      loads: loadsDriver,
    });
  } else {
    let loadsShipper = await Load.find({ created_by: req.user.userId }, '-__v').then(
      (loadsShipper) => {
        res.status(200).json({
          loads: loadsShipper,
        });
      },
    );
  }
};

const createLoad = async (req, res, next) => {
  try {
    const { name, payload, pickup_address, delivery_address, dimensions } = req.body;

    await validateLoad.validateAsync({
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
    });

    const load = new Load({
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions
    });

    load.created_by = req.user.userId;

    await load.save();


    return await res.status(200).json({
      message: 'Load created successfully',
    });

  } catch (error) {
    res.status(500).json({
      message: 'Error!',
    });
  }
};

const getLoadActive = async (req, res, next) => {
  try {
    if (req.user.role === 'DRIVER') {
      const activeLoad = await Load.findOne({ assigned_to: req.user.userId }).then((data) => {
        res.status(200).json({
          load: data,
        });
      });
    } else {
      res.status(400).json({ message: 'Denied' });
    }
  } catch (err) {
    res.status(500).json({
      message: 'Error',
    });
  }
};

const updateLoadActive = async (req, res, next) => {
  try {
    if (req.user.role === 'DRIVER') {

      let states = ['En route to Pick Up', 'En route to delivery', 'Arrived to delivery'];

      const activeLoad = await Load.findOne({ assigned_to: req.user.userId });
      if ((await activeLoad.state) === states[0]) {
        const activeLoadNew = await Load.findOneAndUpdate(
          { assigned_to: req.user.userId },
          { $set: { state: states[1] } },
        );
        await activeLoad.save();

        return res.status(200).json({
          message: `Load state changed to ${states[1]}`,
        });
      }
      if ((await activeLoad.state) === states[1]) {
        await Load.findOneAndUpdate(
          { assigned_to: req.user.userId },
          { $set: { state: states[2], status: 'SHIPPED' } },
        );
      }
      await activeLoad.save();

      const truck = Truck.findOneAndUpdate(
        { userId: req.user.userId, status: 'OL' },
        { $set: { status: 'IS' } },
      );

      return res.status(200).json({
        message: `Load state changed to ${states[2]}`,
      });
    } else {
      return res.status(400).json({ message: 'Access is denied' });
    }
  } catch (err) {
    console.log(err.message);
    res.status(500).json({
      message: 'My Server error =' + err.message,
    });
  }
};

async function createLoadById(req, res, next) {
  const loadId = req.params.id;
  const load = await Load.findById(loadId);

  const truckForLoad = getTruckForLoad(
    load.dimensions.width,
    load.dimensions.length,
    load.dimensions.height,
    load.payload,
  );

  const activeTruck = await Truck.findOne({
    status: 'IS',
    assigned_to: { $ne: null },
  });
  console.log(`activeTruck.assigned_t=${await activeTruck.assigned_to}`);
  if (truckForLoad.length <= 1 && (await activeTruck.type) === truckForLoad[0]) {
    await dataUpdate(await activeTruck.assigned_to);
  } else if (
    truckForLoad.length <= 2 &&
    ((await activeTruck.type) === truckForLoad[0] || (await activeTruck.type) === truckForLoad[1])
  ) {
    await dataUpdate(await activeTruck.assigned_to);
  } else if (
    truckForLoad.length <= 3 &&
    ((await activeTruck.type) === truckForLoad[0] ||
      (await activeTruck.type) === truckForLoad[1] ||
      (await activeTruck.type) === truckForLoad[2])
  ) {
    await dataUpdate(await activeTruck.assigned_to);
  } else {
    await Load.findByIdAndUpdate(
      { _id: loadId },
      {
        $set: {
          status: 'NEW',
          logs: {
            message: 'Driver was not found',
            time: new Date(Date.now()),
          },
        },
      },
    );
  }

  async function dataUpdate(idDriver) {
    await activeTruck.updateOne({
      $set: { status: 'OL' },
    });
    await Load.findByIdAndUpdate(
      { _id: loadId },
      {
        $set: {
          status: 'ASSIGNED',
          state: 'En route to Pick Up',
          assigned_to: idDriver,
          logs: {
            message: `Load assigned to driver with id ${idDriver}`,
            time: new Date(Date.now()),
          },
        },
      },
    );
    return res.json({
      message: 'Load posted successfully',
      driver_found: true,
    });
  }
}

const getLoadById = async (req, res, next) => {
  try {
    await Load.findById(req.params.id).then((load) => {
      res.status(200).json(load);
    });
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ message: 'My Server error' });
  }
};

const updateMyLoadById = async (req, res, next) => {
  try {
    const { name, payload, pickup_address, delivery_address, dimensions } = req.body;
    const loadCurrent = await Load.findById({ _id: req.params.id, created_by: req.user.userId });
    if ((await loadCurrent.status) === 'NEW') {
      await Load.findByIdAndUpdate(
        { _id: req.params.id, created_by: req.user.userId },
        {
          $set: {
            name,
            payload,
            pickup_address,
            delivery_address,
            dimensions,
          },
        },
      ).then((result) => {
        res.status(200).json({
          message: 'Load details changed successfully',
        });
      });
    } else {
      res.status(400).json({
        message: 'Load details cannot be changed',
      });
    }
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ message: 'My Server error' });
  }
};

const deleteLoadById = async (req, res, next) => {
  try {
    const loadCurrent = await Load.findById({ _id: req.params.id, created_by: req.user.userId });
    if ((await loadCurrent.status) === 'NEW') {
      await Load.findByIdAndDelete(req.params.id).then(() => {
        res.status(200).json({
          message: 'Load deleted successfully',
        });
      });
    } else {
      res.status(400).json({
        message: 'Load cannot be deleted',
      });
    }
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ message: 'My Server error' });
  }
};

const getShippindInfo = async (req, res, next) => {
  try {
    console.log(req.params.id);
    const truckCurrent = await Truck.findOne({ status: 'OL' });

    const loadCurrent = await Load.findById({ _id: req.params.id });

    return await res.status(200).json({
      load: loadCurrent,
      truck: truckCurrent,
    });
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ message: 'My Server error' });
  }
};

module.exports = {
  getLoad,
  createLoad,
  getLoadActive,
  updateLoadActive,
  getLoadById,
  createLoadById,
  updateMyLoadById,
  deleteLoadById,
  getShippindInfo,
};
