const bcrypt = require('bcryptjs');
const { User, userJoiSchema } = require('../models/User');

const getUserProfile = async (req, res, next) => {
  try {
    const {
      _id, email, role, createdDate,
    } = await User.findOne({ _id: req.user.userId });
    res.status(200).json({
      user: {
        _id,
        email,
        role,
        createdDate,
      },
    });
  } catch (error) {
    res.status(400).json({
      message: 'This user does not exist',
    });
  }
};

async function editPasswordUser(req, res, next) {
  try {
    const { oldPassword } = req.body;
    const { newPassword } = req.body;
    const { userId, email } = req.user;
    const user = await User.findOne({ email });
    console.log(4444);

    if (oldPassword && newPassword && await bcrypt.compare(
      String(oldPassword),
      String(user.password),
    )) {
      const newPasswordHash = await bcrypt.hash(newPassword, 10);
      console.log(`newPasswordHash=${newPasswordHash}`);
      await User.findByIdAndUpdate(userId, { $set: { password: newPasswordHash } });
      res.status(200).json({
        message: 'Password changed successfully',
      });
    } else {
      res.status(400).json({ message: 'Password not changed' });
    }
  } catch (error) {
    console.log(`error=${error.message}`);
    res.status(500).json({
      message: 'My Server error',
    });
  }
}

async function deleteUser(req, res, next) {
  try {
    const { user } = req;
    await User.deleteOne(user);

    if (!user) {
      res.status(400).json({ message: 'This user does not exist' });
    }
    res.status(200).json({
      message: 'Success',
    });
  } catch (err) {
    res.status(400).json({
      message: 'Server error',
    });
  }
}

module.exports = {
  getUserProfile,
  editPasswordUser,
  deleteUser,
};
