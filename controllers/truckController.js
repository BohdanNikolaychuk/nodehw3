const { Truck } = require('../models/Truck');

function createTrucks(req, res, next) {
  try {
    const { type } = req.body;
    if (!type) {
      res.status(400).json({ message: 'The field must not be empty' });
    }
    const truck = new Truck({
      created_by: req.user.userId,
      type,
      status: 'IS',

    });
    truck.save().then(() => {
      res.status(200).json(
        {
          message: 'Truck created successfully',
        },
      );
    });
  } catch (error) {
    res.status(500).json({
      message: 'Server error',
    });
  }
}

async function getTrucks(req, res, next) {
  return Truck.find({ created_by: req.user.userId }, '-__v')
    .then((trucks) => {
      res.send({
        trucks,
      });
    });
}

const getTruck = async (req, res, next) => {
  try {
    const truck = await Truck.findById(req.params.id);
    if (!truck) {
      res.status(400).json({ message: 'This truck not found' });
    }

    res.status(200).json({
      truck,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      message: 'My Server error',
    });
  }
};

const updateMyTruckById = (req, res, next) => {
  const { type } = req.body;
  return Truck.findByIdAndUpdate(
    { _id: req.params.id, userId: req.user.userId },
    { $set: { type } },
  )
    .then(() => {
      res.status(400).json({ message: 'Truck details changed successfully' });
    });
};

const deleteTruck = async (req, res, next) => await Truck.findByIdAndDelete(req.params.id)
  .then(() => {
    res.status(400).json({
      message: 'Truck deleted successfully',
    });
  });

const assignTruckById = async (req, res, next) => {
  try {
    const truckId = req.params.id;

    const truckAssign = await Truck.findByIdAndUpdate(
      { _id: truckId },
      { $set: { assigned_to: req.user.userId } },
    );
    await truckAssign.save()
      .then(() => {
        res.status(200).json({
          message: 'Truck assigned successfully',
        });
      });
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ message: 'My Server error' });
  }
};

module.exports = {
  createTrucks,
  getTrucks,
  getTruck,
  updateMyTruckById,
  deleteTruck,
  assignTruckById,
};
