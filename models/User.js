const mongoose = require('mongoose');
const Joi = require('joi');


const User = mongoose.model('User', {
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
    unique: true,
  },
  role: {
    type: String,
    require: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },

});


const userValide = Joi.object({
  email: Joi.string()
    .required().email(),
  password: Joi.string()
    .required().min(3)
    .max(30),
  role: Joi.string().valid('DRIVER', 'SHIPPER'),
})


module.exports = {
  User,
  userValide
};
